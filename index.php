<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " .  $sheep->cold_blooded . "<br><br><br>";

$kodok = new Frog("Buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " .  $kodok->cold_blooded . "<br>";
echo $kodok->Jump() . "<br><br><br>";

$sunggokong = new Ape("Kera Sakti");
echo "Name : " . $sunggokong->name . "<br>";
echo "Legs : " . $sunggokong->legs . "<br>";
echo "Cold Blooded : " .  $sunggokong->cold_blooded . "<br>";
echo $sunggokong->Yell();
